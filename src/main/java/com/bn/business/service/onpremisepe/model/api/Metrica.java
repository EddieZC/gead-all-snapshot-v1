package com.bn.business.service.onpremisepe.model.api;

public class Metrica {

	private int id;

	private double porcentajeAvance;
	
	private double costo;
	
	private String status;
	
	private String proyecto;

	public Metrica(int id, double porcentajeAvance, double costo, String status, String proyecto) {
		super();
		this.id = id;
		this.porcentajeAvance = porcentajeAvance;
		this.costo = costo;
		this.status = status;
		this.proyecto = proyecto;
	}

	public Metrica() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPorcentajeAvance() {
		return porcentajeAvance;
	}

	public void setPorcentajeAvance(double porcentajeAvance) {
		this.porcentajeAvance = porcentajeAvance;
	}

	public double getCosto() {
		return costo;
	}

	public void setCosto(double costo) {
		this.costo = costo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getProyecto() {
		return proyecto;
	}

	public void setProyecto(String proyecto) {
		this.proyecto = proyecto;
	}
	
}
