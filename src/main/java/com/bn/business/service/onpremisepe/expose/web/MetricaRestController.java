package com.bn.business.service.onpremisepe.expose.web;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.bn.business.service.onpremisepe.model.api.Metrica;
import com.bn.business.service.onpremisepe.model.api.MetricaGeneral;
import com.bn.business.service.onpremisepe.service.MetricaService;

@RestController
@RequestMapping("/v1")
public class MetricaRestController {
	
	@Autowired
	private MetricaService service;

	// GET /Metricas - HTTP CODE 200
	@GetMapping(value="/metrica", headers="X-PERSISTENCE=db")			// Headers Versioning
	public MetricaGeneral getListarMetricas(){
		System.out.println("entrando ****************************");
		return service.getListarMetricaGeneral();
			
	}
	@GetMapping(value="/{codProyect}", headers="X-PERSISTENCE=db")	
	public Metrica listarPorProyecto(@PathVariable String codProyect){
		return service.listarPorProyecto(codProyect);
//		return null;
	}
}
