package com.bn.business.service.onpremisepe.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.bn.business.service.onpremisepe.model.api.Metrica;
import com.bn.business.service.onpremisepe.model.api.MetricaGeneral;

public interface MetricaService {

	Metrica getHistoryMetricasById(int Id);

	List<Metrica> getListarMetricas();

	MetricaGeneral getListarMetricaGeneral();

	Metrica listarPorProyecto(String codProyect);

}
