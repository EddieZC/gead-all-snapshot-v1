package com.bn.business.service.onpremisepe.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.bn.business.service.onpremisepe.exceptions.MetricaNotFoundException;
import com.bn.business.service.onpremisepe.model.api.Metrica;
import com.bn.business.service.onpremisepe.model.api.MetricaGeneral;
import com.bn.business.service.onpremisepe.service.MetricaService;
import com.bn.business.service.onpremisepe.utils.constants.Constants;

@Component
public class MetricasServiceImpl implements MetricaService{
	
	@Override
	public List<Metrica> getListarMetricas() {
		return ejecutarRest(Constants.URL_PERU);
	}

	@Override
	public Metrica getHistoryMetricasById(int Id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MetricaGeneral getListarMetricaGeneral() {
		List<Metrica> peru= ejecutarRest(Constants.URL_PERU);
		List<Metrica> chile= ejecutarRest(Constants.URL_CHILE);
		List<Metrica> uruguay= ejecutarRest(Constants.URL_URUGUAY);
		MetricaGeneral general=new MetricaGeneral();
		general.setSedePeru(peru);
		general.setSedeChile(chile);
		general.setSedeUruguay(uruguay);
		return general;
	}
	
	private List<Metrica> ejecutarRest(String url) {
		RestTemplate restTemplate =  new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-PERSISTENCE", "db"); 
		HttpEntity<String> entity = new HttpEntity<>("body", headers);
		System.out.println("enity ****************************");
		ResponseEntity<List<Metrica>> response = restTemplate.exchange(url,HttpMethod.GET,
				  entity,new ParameterizedTypeReference<List<Metrica>>(){});
		System.out.println("URL : "+url+" response **************************** "+response);
		List<Metrica> listas = response.getBody();
		return listas;
	}
	
	private Metrica buscarCodProyecto(List<Metrica> lista, String codProyecto) {
		Metrica m= new Metrica();
		for (Metrica metrica : lista) {
			if(metrica.getProyecto().equals(codProyecto)) {
				m= metrica;				
			}
		}
		return m;
	}

	@Override
	public Metrica listarPorProyecto(String codProyect) {
		List<Metrica> peru= ejecutarRest(Constants.URL_PERU);
		List<Metrica> chile= ejecutarRest(Constants.URL_CHILE);
		List<Metrica> uruguay= ejecutarRest(Constants.URL_URUGUAY);
		
		Metrica mPeru= buscarCodProyecto(peru,codProyect);
		Metrica mChile= buscarCodProyecto(chile,codProyect);
		Metrica mUruguay= buscarCodProyecto(uruguay,codProyect);
		
		Metrica general=new Metrica();		
		general.setCosto(mPeru.getCosto()+mChile.getCosto()+mUruguay.getCosto());
		double totalPorcentaje = mPeru.getPorcentajeAvance()+mChile.getPorcentajeAvance()+mUruguay.getPorcentajeAvance() ;
		
		general.setPorcentajeAvance( redondearDecimales(totalPorcentaje/3,2 ) );
		general.setProyecto(codProyect);
		return general;
	}
	
	public static double redondearDecimales(double valorInicial, int numeroDecimales) {
        double parteEntera, resultado;
        resultado = valorInicial;
        parteEntera = Math.floor(resultado);
        resultado=(resultado-parteEntera)*Math.pow(10, numeroDecimales);
        resultado=Math.round(resultado);
        resultado=(resultado/Math.pow(10, numeroDecimales))+parteEntera;
        return resultado;
    }

}
