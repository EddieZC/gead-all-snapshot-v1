package com.bn.business.service.onpremisepe.model.api;

import java.util.List;

public class MetricaGeneral {

	private List<Metrica> sedePeru;
	private List<Metrica> sedeChile;
	private List<Metrica> sedeUruguay;
	public List<Metrica> getSedePeru() {
		return sedePeru;
	}
	public void setSedePeru(List<Metrica> sedePeru) {
		this.sedePeru = sedePeru;
	}
	public List<Metrica> getSedeChile() {
		return sedeChile;
	}
	public void setSedeChile(List<Metrica> sedeChile) {
		this.sedeChile = sedeChile;
	}
	public List<Metrica> getSedeUruguay() {
		return sedeUruguay;
	}
	public void setSedeUruguay(List<Metrica> sedeUruguay) {
		this.sedeUruguay = sedeUruguay;
	}
	
}
