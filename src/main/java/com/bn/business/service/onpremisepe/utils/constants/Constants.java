package com.bn.business.service.onpremisepe.utils.constants;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;

public class Constants {
	
	public static int ALL_HISTORY= 10;
	
	public static final Contact DEFAULT_CONTACT = new Contact(
			"Edi Castro", 
			"http://ecastro.architect.com.pe", 
			"ecastroyllescas@gmail.com");
	
	public static final ApiInfo DEFAULT_API_INFO = new ApiInfo(
			"Rent API Documentation", 
			"WADL - Swagger contract RESTful Web Service API", 
			"v1", 
			"urn:com.bn.business.service.rents",
			DEFAULT_CONTACT, 
			"Apache 2.0", 
			"http://www.apache.org/licenses/LICENSE-2.0");

	public static final Set<String> DEFAULT_PRODUCES_AND_CONSUMERS = new HashSet<String>(Arrays.asList("application/json","application/xml"));

	public static final String URL_PERU = "http://localhost:8080/v1/metrica";
	public static final String URL_CHILE = "http://localhost:8081/v1/metrica";
	public static final String URL_URUGUAY = "http://localhost:8082/v1/metrica";
	
}
